#!/usr/bin/env bash

apt-get update
#dev environment
apt-get install -y gcc make gdb g++ git cmake valgrind

#set up ntp clock sync
timedatectl set-ntp true
timedatectl set-timezone "Europe/London"
