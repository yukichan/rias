#ifndef LIBRIAS_MESSAGE_H
#define LIBRIAS_MESSAGE_H

#include <cstddef>
#include "arduino_json.h"

typedef enum MessageType_t {
  //Status messages
  STATUS_OK_MESSAGE,
  STATUS_ERROR_MESSAGE,
  STATUS_PING_MESSAGE,
  STATUS_TEST_MESSAGE,
  STATUS_REQUEST_LOG_MESSAGE,
  STATUS_LOG_MESSAGE,
  //Unknown message type
  UNKNOWN_MESSAGE
} MessageType_t;
MessageType_t parse_message_type(const char* type);

class Message {
public:
  MessageType_t type;
  unsigned int id;

  virtual ~Message() = 0;
  //Takes a json string and deserializes it to a Message object
  static Message* parse_message(char* message);
  //Serializes the message to a JSON string and writes it to the terget buffer
  virtual void dump_message(char* target, size_t size) = 0;
  //To be implemented by subclasses: takes a json string and deserializes it to
  //the current object
  virtual bool parse_content(ArduinoJson::JsonObject& message) = 0;
};

class NullMessage : public Message {
public:
  NullMessage();
  ~NullMessage() {};
  void dump_message(char* target, size_t size) {(void)target; (void)size;}
  bool parse_content(ArduinoJson::JsonObject& message) {(void)message; return false;}
};

#endif
