#ifndef LIBRIAS_CONFIG_H
#define LIBRIAS_CONFIG_H

#ifndef JSON_BUFFER_SIZE
#define JSON_BUFFER_SIZE 256
#endif

#ifndef JSON_NESTING_DEPTH
#define JSON_NESTING_DEPTH 10
#endif

#ifndef ERROR_STRING_BUFFER_SIZE
#define ERROR_STRING_BUFFER_SIZE 128
#endif

#ifndef TEST_STRING_BUFFER_SIZE
#define TEST_STRING_BUFFER_SIZE 128
#endif

#ifndef LOG_STRING_BUFFER_SIZE
#define LOG_STRING_BUFFER_SIZE 128
#endif

#endif //LIBRIAS_CONFIG_H
