#include "config.h"
#include "status_message.h"
#include "arduino_json.h"

//--------------------------------------------
// OK Status Message
//--------------------------------------------
OkMessage::OkMessage(unsigned int id) {
  this->id = id;
}

void OkMessage::dump_message(char* target, size_t size) {
  StaticJsonBuffer<JSON_BUFFER_SIZE> json_buffer;
  JsonArray& root_arr = json_buffer.createArray();
  JsonObject& root_obj = root_arr.createNestedObject();
  JsonObject& contents_obj = root_obj.createNestedObject("Ok");
  contents_obj["Id"] = this->id;
  root_arr.printTo(target, size);
  return;
}

bool OkMessage::parse_content(JsonObject& message) {
  this->type = STATUS_OK_MESSAGE;
  if (message.containsKey("Id") && message.get<JsonVariant>("Id").is<unsigned int>()) {
    this->id = message.get<unsigned int>("Id");
    return true;
  } else {
    return false;
  }
}

//--------------------------------------------
// Error Status Message
//--------------------------------------------
ErrorMessage::ErrorMessage(unsigned int id, const char* err_string, int err_code) {
  this->id = id;
  strncpy(this->err_string, err_string, ERROR_STRING_BUFFER_SIZE);
  this->err_code = err_code;
}

void ErrorMessage::dump_message(char* target, size_t size) {
  StaticJsonBuffer<JSON_BUFFER_SIZE> json_buffer;
  JsonArray& root_arr = json_buffer.createArray();
  JsonObject& root_obj = root_arr.createNestedObject();
  JsonObject& contents_obj = root_obj.createNestedObject("Error");
  contents_obj["Id"] = this->id;
  contents_obj["ErrorMessage"] = this->err_string;
  contents_obj["ErrorCode"] = this->err_code;
  root_arr.printTo(target, size);
  return;
}

bool ErrorMessage::parse_content(JsonObject& message) {
  this->type = STATUS_ERROR_MESSAGE;
  if (message.containsKey("Id") && message.get<JsonVariant>("Id").is<unsigned int>()
   && message.containsKey("ErrorMessage") && message.get<JsonVariant>("ErrorMessage").is<const char*>()
   && message.containsKey("ErrorCode") && message.get<JsonVariant>("ErrorCode").is<signed int>()) {
    this->id = message.get<unsigned int>("Id");
    const char* err_str = message.get<const char*>("ErrorMessage");
    strncpy(this->err_string, err_str, ERROR_STRING_BUFFER_SIZE);
    this->err_code = message.get<signed int>("ErrorCode");
    return true;
  }
  return false;
}

//--------------------------------------------
// Ping Status Message
//--------------------------------------------
PingMessage::PingMessage(unsigned int id) {
  this->id = id;
}

void PingMessage::dump_message(char* target, size_t size) {
  StaticJsonBuffer<JSON_BUFFER_SIZE> json_buffer;
  JsonArray& root_arr = json_buffer.createArray();
  JsonObject& root_obj = root_arr.createNestedObject();
  JsonObject& contents_obj = root_obj.createNestedObject("Ping");
  contents_obj["Id"] = this->id;
  root_arr.printTo(target, size);
  return;
}

bool PingMessage::parse_content(JsonObject& message) {
  this->type = STATUS_PING_MESSAGE;
  if (message.containsKey("Id") && message.get<JsonVariant>("Id").is<unsigned int>()) {
    this->id = message.get<unsigned int>("Id");
    return true;
  } else {
    return false;
  }
}

//--------------------------------------------
// Test Status Message
//--------------------------------------------
TestMessage::TestMessage(unsigned int id, const char* test_string) {
  this->id = id;
  strncpy(this->test_msg, test_string, TEST_STRING_BUFFER_SIZE);
}

void TestMessage::dump_message(char* target, size_t size) {
  StaticJsonBuffer<JSON_BUFFER_SIZE> json_buffer;
  JsonArray& root_arr = json_buffer.createArray();
  JsonObject& root_obj = root_arr.createNestedObject();
  JsonObject& contents_obj = root_obj.createNestedObject("Test");
  contents_obj["Id"] = this->id;
  contents_obj["TestString"] = this->test_msg;
  root_arr.printTo(target, size);
  return;
}

bool TestMessage::parse_content(JsonObject& message) {
  this->type = STATUS_TEST_MESSAGE;
  if (message.containsKey("Id") && message.get<JsonVariant>("Id").is<unsigned int>()
   && message.containsKey("TestString") && message.get<JsonVariant>("TestString").is<const char*>()) {
    this->id = message.get<unsigned int>("Id");
    const char* test_str = message.get<const char*>("TestString");
    strncpy(this->test_msg, test_str, TEST_STRING_BUFFER_SIZE);
    return true;
  }
  return false;
}

//--------------------------------------------
// RequestLog Status Message
//--------------------------------------------

LogLevel_t parse_log_level(const char* level) {
  if (strcmp("Off", level) == 0) {
    return OFF;
  } else if (strcmp("Fatal", level) == 0) {
    return FATAL;
  } else if (strcmp("Error", level) == 0) {
    return ERROR;
  } else if (strcmp("Warn", level) == 0) {
    return WARN;
  } else if (strcmp("Info", level) == 0) {
    return INFO;
  } else if (strcmp("Debug", level) == 0) {
    return DEBUG;
  } else if (strcmp("Trace", level) == 0) {
    return TRACE;
  } else {
    return INVALID;
  }
}

const char* log_level_to_str(LogLevel_t level) {
  switch(level) {
    case OFF: return "Off";
    case FATAL: return "Fatal";
    case ERROR: return "Error";
    case WARN: return "Warn";
    case INFO: return "Info";
    case DEBUG: return "Debug";
    case TRACE: return "Trace";
    default: return NULL;
  }
}

RequestLogMessage::RequestLogMessage(unsigned int id, LogLevel_t level) {
  this->id = id;
  this->requested_level = level;
}

void RequestLogMessage::dump_message(char* target, size_t size) {
  StaticJsonBuffer<JSON_BUFFER_SIZE> json_buffer;
  JsonArray& root_arr = json_buffer.createArray();
  JsonObject& root_obj = root_arr.createNestedObject();
  JsonObject& contents_obj = root_obj.createNestedObject("RequestLog");
  contents_obj["Id"] = this->id;
  const char* log_level_str = log_level_to_str(this->requested_level);
  if (log_level_str == NULL) return;
  contents_obj["LogLevel"] = log_level_str;
  root_arr.printTo(target, size);
  return;
}

bool RequestLogMessage::parse_content(JsonObject& message) {
  this->type = STATUS_REQUEST_LOG_MESSAGE;
  if (message.containsKey("Id") && message.get<JsonVariant>("Id").is<unsigned int>()
   && message.containsKey("LogLevel") && message.get<JsonVariant>("LogLevel").is<const char*>()) {
    this->id = message.get<unsigned int>("Id");
    const char* level = message.get<const char*>("LogLevel");
    this->requested_level = parse_log_level(level);
    return (this->requested_level == INVALID) ? false : true;
  }
  return false;
}

//--------------------------------------------
// Log Status Message
//--------------------------------------------
LogMessage::LogMessage(unsigned int id, LogLevel_t level, const char* log_msg) {
  this->id = id;
  this->level = level;
  strncpy(this->log_msg, log_msg, LOG_STRING_BUFFER_SIZE);
}

void LogMessage::dump_message(char* target, size_t size) {
  StaticJsonBuffer<JSON_BUFFER_SIZE> json_buffer;
  JsonArray& root_arr = json_buffer.createArray();
  JsonObject& root_obj = root_arr.createNestedObject();
  JsonObject& contents_obj = root_obj.createNestedObject("Log");
  contents_obj["Id"] = this->id;
  contents_obj["LogLevel"] = log_level_to_str(this->level);
  contents_obj["LogMessage"] = this->log_msg;
  root_arr.printTo(target, size);
  return;
}

bool LogMessage::parse_content(JsonObject& message) {
  this->type = STATUS_LOG_MESSAGE;
  if (message.containsKey("Id") && message.get<JsonVariant>("Id").is<unsigned int>()
   && message.containsKey("LogLevel") && message.get<JsonVariant>("LogLevel").is<const char*>()
   && message.containsKey("LogMessage") && message.get<JsonVariant>("LogMessage").is<const char*>()) {
    this->id = message.get<unsigned int>("Id");
    const char* level = message.get<const char*>("LogLevel");
    this->level = parse_log_level(level);
    const char* log_str = message.get<const char*>("LogMessage");
    strncpy(this->log_msg, log_str, LOG_STRING_BUFFER_SIZE);
    return true;
  }
  return false;
}
