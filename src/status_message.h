#ifndef LIBRIAS_STATUS_MESSAGE_H
#define LIBRIAS_STATUS_MESSAGE_H

#include "config.h"
#include "message.h"

typedef enum LogLevel_t {
  OFF,
  FATAL,
  ERROR,
  WARN,
  INFO,
  DEBUG,
  TRACE,
  INVALID
} LogLevel_t;
LogLevel_t parse_log_level(const char* level);
const char* log_level_to_str(LogLevel_t level);

//https://buttplug-spec.docs.buttplug.io/status.html#ok
class OkMessage: public Message {
public:
  OkMessage() {};
  OkMessage(unsigned int id);
  ~OkMessage() {};
  void dump_message(char* message, size_t size);
  bool parse_content(JsonObject& message);
};

//https://buttplug-spec.docs.buttplug.io/status.html#error
class ErrorMessage: public Message {
public:
  char err_string[ERROR_STRING_BUFFER_SIZE];
  int err_code;

  ErrorMessage() {};
  ErrorMessage(unsigned int id, const char* err_string, int err_code);
  ~ErrorMessage() {};
  void dump_message(char* message, size_t size);
  bool parse_content(JsonObject& message);
};

//https://buttplug-spec.docs.buttplug.io/status.html#ping
class PingMessage: public Message {
public:
  PingMessage() {};
  PingMessage(unsigned int id);
  ~PingMessage() {};
  void dump_message(char* message, size_t size);
  bool parse_content(JsonObject& message);
};

//https://buttplug-spec.docs.buttplug.io/status.html#test
class TestMessage: public Message {
public:
  char test_msg[TEST_STRING_BUFFER_SIZE];

  TestMessage() {};
  TestMessage(unsigned int id, const char* test_msg);
  ~TestMessage() {};
  void dump_message(char* message, size_t size);
  bool parse_content(JsonObject& message);
};

//https://buttplug-spec.docs.buttplug.io/status.html#requestlog
class RequestLogMessage: public Message {
public:
  LogLevel_t requested_level;

  RequestLogMessage() {};
  RequestLogMessage(unsigned int id, LogLevel_t level);
  ~RequestLogMessage() {};
  void dump_message(char* message, size_t size);
  bool parse_content(JsonObject& message);
};

//https://buttplug-spec.docs.buttplug.io/status.html#log
class LogMessage: public Message {
public:
  LogLevel_t level;
  char log_msg[LOG_STRING_BUFFER_SIZE];

  LogMessage() {};
  LogMessage(unsigned int id, LogLevel_t level, const char* message);
  ~LogMessage() {};
  void dump_message(char* message, size_t size);
  bool parse_content(JsonObject& message);
};

#endif
