#include "message.h"
#include "status_message.h"
#include "arduino_json.h"
#include "config.h"

#include <string.h>
#include <cstddef>

Message* Message::parse_message(char* message) {
  StaticJsonBuffer<JSON_BUFFER_SIZE> jsonBuffer;
  JsonArray& root = jsonBuffer.parseArray(message, JSON_NESTING_DEPTH);
  JsonObject& rootObj = root.begin()->as<JsonObject&>();
  //The only contents of the object should be a key depending on the
  //type of the message and an object containing the data.
  const char* messageType = rootObj.begin()->key;
  JsonObject& messageContent = rootObj.begin()->value.as<JsonObject&>();
  MessageType_t type = parse_message_type(messageType);

  Message* msg;
  switch(type) {
    case STATUS_OK_MESSAGE: {
      msg = new OkMessage;
      break;
    }
    case STATUS_ERROR_MESSAGE: {
      msg = new ErrorMessage;
      break;
    }
    case STATUS_PING_MESSAGE: {
      msg = new PingMessage;
      break;
    }
    case STATUS_TEST_MESSAGE: {
      msg = new TestMessage;
      break;
    }
    case STATUS_REQUEST_LOG_MESSAGE: {
      msg = new RequestLogMessage;
      break;
    }
    case STATUS_LOG_MESSAGE: {
      msg = new LogMessage;
      break;
    }
    case UNKNOWN_MESSAGE:
    default:
    return NULL;
  }
  if (msg->parse_content(messageContent)) {
    return msg;
  } else {
    delete(msg);
    return NULL;
  }

}

MessageType_t parse_message_type(const char* type) {
  if (strcmp(type, "Ok") == 0) {
    return STATUS_OK_MESSAGE;
  } else if (strcmp(type, "Error") == 0) {
    return STATUS_ERROR_MESSAGE;
  } else if (strcmp(type, "Ping") == 0) {
    return STATUS_PING_MESSAGE;
  } else if (strcmp(type, "Test") == 0) {
    return STATUS_TEST_MESSAGE;
  } else if (strcmp(type, "RequestLog") == 0) {
    return STATUS_REQUEST_LOG_MESSAGE;
  } else if (strcmp(type, "Log") == 0) {
    return STATUS_LOG_MESSAGE;
  } else {
    return UNKNOWN_MESSAGE;
  }
}

Message::~Message() {}
