#include "gtest/gtest.h"
#include "stacktrace.h"

#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>



void handler(int sig) {
  void *array[10];
  print_stacktrace();
  exit(1);
}

int main(int argc, char **argv) {
  signal(SIGSEGV, handler);
  ::testing::InitGoogleTest(&argc, argv);
  int ret = RUN_ALL_TESTS();
  return ret;
}
