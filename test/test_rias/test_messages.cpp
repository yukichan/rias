#include "rias.h"
#include "gtest/gtest.h"

#define STR_BUFFER_SIZE 256


//Ensures that the library can corrently parse a valid JSON string into
//a message object.
TEST(OkMessageTest, ReadsValidJsonInput) {
  const char* json_str = "[{\"Ok\": {\"Id\": 69}}]";
  char test_str[STR_BUFFER_SIZE]; strncpy(test_str, json_str, strlen(json_str));
  OkMessage* res = dynamic_cast<OkMessage*>(Message::parse_message(test_str));
  EXPECT_EQ(69, res->id);
  EXPECT_EQ(STATUS_OK_MESSAGE, res->type);
  delete(res);
}

//Ensures that the library handles an invalid json string in a reasonable
//manner.
TEST(OkMessageTest, ReadsInvalidTypedJsonInput) {
  const char* json_str = "[{\"Ok\": {\"Id\": \"uwot\"}}]";
  char test_str[STR_BUFFER_SIZE]; strncpy(test_str, json_str, strlen(json_str));
  Message* res = Message::parse_message(test_str);
  EXPECT_EQ(NULL, res);
}

//Ensures the library can correctly output a json representation of an
//OkMessage object.
TEST(OkMessageTest, OutputsValidJson) {
  const char* json_str = "[{\"Ok\":{\"Id\":69}}]";
  char target_buffer[STR_BUFFER_SIZE];
  OkMessage test_msg = OkMessage(69);
  test_msg.dump_message(target_buffer, STR_BUFFER_SIZE);
  EXPECT_STREQ(json_str, target_buffer);
}

//Ensures that the library can corrently parse a valid JSON string into
//a message object.
TEST(ErrorMessageTest, ReadsValidJsonInput) {
  const char* json_str = "[{\"Error\": {\"Id\": 69, \"ErrorMessage\": \"This is the error string\", \"ErrorCode\": -69}}]";
  char test_str[STR_BUFFER_SIZE]; strncpy(test_str, json_str, strlen(json_str));
  ErrorMessage* res = dynamic_cast<ErrorMessage*>(Message::parse_message(test_str));
  EXPECT_EQ(69, res->id);
  EXPECT_EQ(STATUS_ERROR_MESSAGE, res->type);
  EXPECT_STREQ("This is the error string", res->err_string);
  EXPECT_EQ(-69, res->err_code);
  delete(res);
}

//Ensures that the library handles an invalid json string in a reasonable
//manner.
TEST(ErrorMessageTest, ReadsInvalidTypedJsonInput) {
  const char* json_str = "[{\"Error\": {\"Id\": \"uwot\"}}]";
  char test_str[STR_BUFFER_SIZE]; strncpy(test_str, json_str, strlen(json_str));
  Message* res = Message::parse_message(test_str);
  EXPECT_EQ(NULL, res);
}

//Ensures the library can correctly output a json representation of an
//ErrorMessage object.
TEST(ErrorMessageTest, OutputsValidJson) {
  const char* json_str = "[{\"Error\":{\"Id\":69,\"ErrorMessage\":\"test_err_string\",\"ErrorCode\":1}}]";
  char target_buffer[STR_BUFFER_SIZE];
  ErrorMessage test_msg = ErrorMessage(69, "test_err_string", 1);
  test_msg.dump_message(target_buffer, STR_BUFFER_SIZE);
  EXPECT_STREQ(json_str, target_buffer);
}

//Ensures that the library can corrently parse a valid JSON string into
//a message object.
TEST(PingMessageTest, ReadsValidJsonInput) {
  const char* json_str ="[{\"Ping\": {\"Id\": 69}}]";
  char test_str[STR_BUFFER_SIZE]; strncpy(test_str, json_str, strlen(json_str));
  Message* res = Message::parse_message(test_str);
  EXPECT_EQ(69, res->id);
  EXPECT_EQ(STATUS_PING_MESSAGE, res->type);
  delete(res);
}

//Ensures that the library handles an invalid json string in a reasonable
//manner.
TEST(PingMessageTest, ReadsInvalidTypedJsonInput) {
  const char* json_str = "[{\"Ping\": {\"Id\": \"uwot\"}}]";
  char test_str[STR_BUFFER_SIZE]; strncpy(test_str, json_str, strlen(json_str));
  PingMessage* res = dynamic_cast<PingMessage*>(Message::parse_message(test_str));
  EXPECT_EQ(NULL, res);
}

//Ensures the library can correctly output a json representation of a
//PingMessage object.
TEST(PingMessageTest, OutputsValidJson) {
  const char* json_str = "[{\"Ping\":{\"Id\":69}}]";
  char target_buffer[STR_BUFFER_SIZE];
  PingMessage test_msg = PingMessage(69);
  test_msg.dump_message(target_buffer, STR_BUFFER_SIZE);
  EXPECT_STREQ(json_str, target_buffer);
}

//Ensures that the library can corrently parse a valid JSON string into
//a message object.
TEST(TestMessageTest, ReadsValidJsonInput) {
  const char* json_str ="[{\"Test\": {\"Id\": 69,\"TestString\":\"butts\"}}]";
  char test_str[STR_BUFFER_SIZE]; strncpy(test_str, json_str, strlen(json_str));
  TestMessage* res = dynamic_cast<TestMessage*>(Message::parse_message(test_str));
  EXPECT_EQ(69, res->id);
  EXPECT_EQ(STATUS_TEST_MESSAGE, res->type);
  EXPECT_STREQ("butts", res->test_msg);
  delete(res);
}

//Ensures that the library handles an invalid json string in a reasonable
//manner.
TEST(TestMessageTest, ReadsInvalidTypedJsonInput) {
  const char* json_str = "[{\"Test\": {\"Id\": \"uwot\"}}]";
  char test_str[STR_BUFFER_SIZE]; strncpy(test_str, json_str, strlen(json_str));
  Message* res = Message::parse_message(test_str);
  EXPECT_EQ(NULL, res);
}

//Ensures the library can correctly output a json representation of a
//TestMessage object.
TEST(TestMessageTest, OutputsValidJson) {
  const char* json_str = "[{\"Test\":{\"Id\":69,\"TestString\":\"butts\"}}]";
  char target_buffer[STR_BUFFER_SIZE];
  TestMessage test_msg = TestMessage(69, "butts");
  test_msg.dump_message(target_buffer, STR_BUFFER_SIZE);
  EXPECT_STREQ(json_str, target_buffer);
}

//Ensures that the library can corrently parse a valid JSON string into
//a message object.
TEST(RequestLogMessageTest, ReadsValidJsonInput) {
  const char* json_str ="[{\"RequestLog\": {\"Id\": 69,\"LogLevel\":\"Trace\"}}]";
  char test_str[STR_BUFFER_SIZE]; strncpy(test_str, json_str, strlen(json_str));
  RequestLogMessage* res = dynamic_cast<RequestLogMessage*>(Message::parse_message(test_str));
  EXPECT_EQ(69, res->id);
  EXPECT_EQ(STATUS_REQUEST_LOG_MESSAGE, res->type);
  EXPECT_EQ(TRACE, res->requested_level);
  delete(res);
}

//Ensures that the library handles an invalid json string in a reasonable
//manner.
TEST(RequestLogMessageTest, ReadsInvalidTypedJsonInput) {
  const char* json_str = "[{\"RequestLog\": {\"Id\": \"uwot\",\"LogLevel\":\"Trace\"}}]";
  char test_str[STR_BUFFER_SIZE]; strncpy(test_str, json_str, strlen(json_str));
  Message* res = Message::parse_message(test_str);
  EXPECT_EQ(NULL, res);
}

//Ensures the library can correctly output a json representation of a
//RequestLogMessage object.
TEST(RequestLogMessageTest, OutputsValidJson) {
  const char* json_str = "[{\"RequestLog\":{\"Id\":69,\"LogLevel\":\"Warn\"}}]";
  char target_buffer[STR_BUFFER_SIZE];
  RequestLogMessage test_msg = RequestLogMessage(69, WARN);
  test_msg.dump_message(target_buffer, STR_BUFFER_SIZE);
  EXPECT_STREQ(json_str, target_buffer);
}

//Ensures that the library can corrently parse a valid JSON string into
//a message object.
TEST(LogMessageTest, ReadsValidJsonInput) {
  const char* json_str ="[{\"Log\": {\"Id\": 69,\"LogLevel\":\"Trace\",\"LogMessage\":\"butts\"}}]";
  char test_str[STR_BUFFER_SIZE]; strncpy(test_str, json_str, strlen(json_str));
  LogMessage* res = dynamic_cast<LogMessage*>(Message::parse_message(test_str));
  EXPECT_EQ(69, res->id);
  EXPECT_EQ(STATUS_LOG_MESSAGE, res->type);
  EXPECT_EQ(TRACE, res->level);
  EXPECT_STREQ("butts", res->log_msg);
  delete(res);
}

//Ensures that the library handles an invalid json string in a reasonable
//manner.
TEST(LogMessageTest, ReadsInvalidTypedJsonInput) {
  const char* json_str = "[{\"Log\": {\"Id\": \"uwot\",\"LogLevel\":\"Trace\"}}]";
  char test_str[STR_BUFFER_SIZE]; strncpy(test_str, json_str, strlen(json_str));
  Message* res = Message::parse_message(test_str);
  EXPECT_EQ(NULL, res);
}

//Ensures the library can correctly output a json representation of a
//RequestLogMessage object.
TEST(LogMessageTest, OutputsValidJson) {
  const char* json_str = "[{\"Log\":{\"Id\":69,\"LogLevel\":\"Warn\",\"LogMessage\":\"butts\"}}]";
  char target_buffer[STR_BUFFER_SIZE];
  LogMessage test_msg = LogMessage(69, WARN, "butts");
  test_msg.dump_message(target_buffer, STR_BUFFER_SIZE);
  EXPECT_STREQ(json_str, target_buffer);
}
